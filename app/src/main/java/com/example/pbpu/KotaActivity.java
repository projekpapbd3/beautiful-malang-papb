package com.example.pbpu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pbpu.adapter.ListAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class KotaActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView listView;
    private ImageView btnBack;
    private ArrayList<List> reviewList;
    DatabaseReference dbReview;

    //    Intent getintent = getIntent();
//    String database = getintent.getStringExtra("database");
    String database = "Kota";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kota);

        dbReview = FirebaseDatabase.getInstance().getReference(database);

        listView = findViewById(R.id.lv_list);

        btnBack = findViewById(R.id.back);
        btnBack.setOnClickListener(this);

        reviewList = new ArrayList<>();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(KotaActivity.this, ListWisataActivity.class);
                intent.putExtra(ListWisataActivity.EXTRA_REVIEW, reviewList.get(i));

                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.back) {
            Intent in = new Intent(KotaActivity.this, ListWisataActivity.class);
            startActivity(in);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        String email = currentUser.getEmail();
        dbReview.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                reviewList.clear();

                for (DataSnapshot reviewSnapshot : dataSnapshot.getChildren()) {
                    List review = reviewSnapshot.getValue(List.class);
                    reviewList.add(review);
                }

                ListAdapter adapter = new ListAdapter(KotaActivity.this);
                adapter.setReviewList(reviewList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(KotaActivity.this, "Terjadi kesalahan.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
