package com.example.pbpu;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pbpu.adapter.ListAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListWisataActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView listView;
    private ImageView btnBack;
    private TextView topbar;
    private ArrayList<List> reviewList;
    public static final String EXTRA_REVIEW = "extra_review";

    private List rev;
    private String reviewId;

    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_wisata);

        rev = getIntent().getParcelableExtra(EXTRA_REVIEW);

        if (rev != null) {
            reviewId = rev.getId();
        } else {
            rev = new List();
        }

        mDatabase = FirebaseDatabase.getInstance().getReference("/Kota/"+reviewId+"/list/");
        listView = findViewById(R.id.lv_list);
        btnBack = findViewById(R.id.back);
        btnBack.setOnClickListener(this);
        topbar = findViewById(R.id.tempatWisata);
        reviewList = new ArrayList<>();

        topbar.setText(rev.getId());

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ListWisataActivity.this, DetailWisataActivity.class);
                intent.putExtra(ListWisataActivity.EXTRA_REVIEW, reviewList.get(i));
                intent.putExtra("path",rev.getId());

                startActivity(intent);
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Edit Data");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }



    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.back) {
            Intent in = new Intent(ListWisataActivity.this, DetailWisataActivity.class);
            startActivity(in);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        String email = currentUser.getEmail();
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                reviewList.clear();

                for (DataSnapshot reviewSnapshot : dataSnapshot.getChildren()) {
                    List review = reviewSnapshot.getValue(List.class);
                    reviewList.add(review);
                }

                ListAdapter adapter = new ListAdapter(ListWisataActivity.this);
                adapter.setReviewList(reviewList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ListWisataActivity.this, "Terjadi kesalahan.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
