package com.example.pbpu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailWisataActivity extends AppCompatActivity {

    TextView tvname, tvkategori, tvweb, tvkontak, tvdeskripsi, tvlokasi, tvjarak;
    ImageView ivWisata, ivRating;
    ArrayList<List> reviewList;
    ArrayList<Detail> detailList;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_wisata_activity);

        tvname = findViewById(R.id.namalokasi);
        tvkategori = findViewById(R.id.kategori);
        tvjarak = findViewById(R.id.jarak);
        tvweb = findViewById(R.id.website);
        tvkontak = findViewById(R.id.nomortelp);
        tvdeskripsi = findViewById(R.id.isideskripsi);
        tvlokasi = findViewById(R.id.isilokasi);
        ivWisata = findViewById(R.id.ivWisata);
        ivRating = findViewById(R.id.stars);

        tvweb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent webIntent = new Intent(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(tvweb.getText().toString()));
                if(tvweb.getText().toString().equals("N/A")){
                    Toast.makeText(DetailWisataActivity.this, "Unavailable", Toast.LENGTH_SHORT).show();
                }
                else
                    startActivity(webIntent);
            }
        });

        tvkontak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + tvkontak.getText().toString()));
                if (tvkontak.getText().toString().equals("N/A")) {
                    Toast.makeText(DetailWisataActivity.this, "Unavailable", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(callIntent);
                }
            }
        });

        ivRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String idWisata = getIntent().getStringExtra("ID");
                String idKota = getIntent().getStringExtra("ID_KOTA");
//                String toReview = "/Kota/" + idKota + "/list/" + idWisata;
                Log.d("QWERTY", idWisata);
                Log.d("ASDFG", idKota);
                Intent intent = new Intent(DetailWisataActivity.this, MainReview.class);
                intent.putExtra("path", "/Kota/" + idKota + "/list/" + idWisata);
                startActivity(intent);
            }
        });



        getData();
    }
    private void getData() {

        String idWisata = getIntent().getStringExtra("ID");
        String idKota = getIntent().getStringExtra("ID_KOTA");

        databaseReference = FirebaseDatabase.getInstance().getReference("/Kota/" + idKota + "/list/" + idWisata);

        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Detail detailWisata = snapshot.getValue(Detail.class);

                if (detailWisata != null) {
                    tvname.setText(detailWisata.getId());
                    tvkategori.setText("Wisata " + detailWisata.getKategori());
                    tvjarak.setText("~" + detailWisata.getJarak() + " km");
                    tvweb.setText(detailWisata.getWebsite());
                    tvkontak.setText(detailWisata.getKontak());
                    tvdeskripsi.setText(detailWisata.getDeskripsi());
                    tvlokasi.setText(detailWisata.getLokasi());
                    Glide.with(DetailWisataActivity.this)
                            .load(detailWisata.getImage())
                            .into(ivWisata);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}