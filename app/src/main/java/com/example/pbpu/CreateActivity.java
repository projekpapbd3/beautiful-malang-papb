package com.example.pbpu;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pbpu.R;
import com.example.pbpu.Review;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CreateActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edtEmail, edtKomentar;
    private Button btnSave;

    private Review review;

    DatabaseReference mDatabase;

//    Intent getintent = getIntent();
//    String path = getintent.getStringExtra("path");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        Intent getintent = getIntent();
        String path = getintent.getStringExtra("path");

        mDatabase = FirebaseDatabase.getInstance().getReference(path);

        edtEmail = findViewById(R.id.edt_email);
        edtKomentar = findViewById(R.id.edt_komentar);
        btnSave = findViewById(R.id.btn_save);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String mail = user.getEmail();

        edtEmail.setText(mail);

        btnSave.setOnClickListener(this);

        review = new Review();
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btn_save) {
            saveReview();
        }

    }

    private void saveReview()
    {
        String email = edtEmail.getText().toString().trim();
        String komentar = edtKomentar.getText().toString().trim();

        boolean isEmptyFields = false;

        if (TextUtils.isEmpty(email)) {
            isEmptyFields = true;
            edtEmail.setError("Field ini tidak boleh kosong");
        }

        if (TextUtils.isEmpty(komentar)) {
            isEmptyFields = true;
            edtKomentar.setError("Field ini tidak boleh kosong");
        }

        if (! isEmptyFields) {

            Toast.makeText(CreateActivity.this, "Saving Data...", Toast.LENGTH_SHORT).show();
//            Intent intent = getIntent();
//            String database = intent.getStringExtra("database");
            DatabaseReference dbReview = mDatabase;

            String id = dbReview.push().getKey();
            review.setId(id);
            review.setEmail(email);
            review.setKomentar(komentar);
            review.setPhoto("");

            //insert data
            dbReview.child(id).setValue(review);

            finish();

        }
    }
}