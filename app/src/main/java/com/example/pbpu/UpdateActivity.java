package com.example.pbpu;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pbpu.R;
import com.example.pbpu.Review;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edtEmail, edtKomentar;
    private Button btnUpdate;

    public static final String EXTRA_REVIEW = "extra_review";
    public final int ALERT_DIALOG_CLOSE = 10;
    public final int ALERT_DIALOG_DELETE = 20;

    private Review review;
    private String reviewId;

    DatabaseReference mDatabase;
//
//    Intent getintent = getIntent();
//    String path = getintent.getStringExtra("path");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        Intent getintent = getIntent();
        String path = getintent.getStringExtra("path");

        mDatabase = FirebaseDatabase.getInstance().getReference(path);

        edtKomentar = findViewById(R.id.edt_edit_komentar);
        edtEmail = findViewById(R.id.edt_edit_email);
        btnUpdate = findViewById(R.id.btn_update);
        btnUpdate.setOnClickListener(this);

        review = getIntent().getParcelableExtra(EXTRA_REVIEW);

        if (review != null) {
            reviewId = review.getId();
        } else {
            review = new Review();
        }

        if (review != null) {
            edtEmail.setText(review.getEmail());
            edtKomentar.setText(review.getKomentar());

        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Edit Data");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_update) {
            updateReview();
        }

    }

    public void updateReview() {
        String komentar = edtKomentar.getText().toString().trim();
        String email = edtEmail.getText().toString().trim();

        boolean isEmptyFields = false;

        if (TextUtils.isEmpty(komentar)) {
            isEmptyFields = true;
            edtKomentar.setError("Field ini tidak boleh kosong");
        }

        if (TextUtils.isEmpty(email)) {
            isEmptyFields = true;
            edtEmail.setError("Field ini tidak boleh kosong");
        }

        if (! isEmptyFields) {

            Toast.makeText(UpdateActivity.this, "Updating Data...", Toast.LENGTH_SHORT).show();

            review.setEmail(email);
            review.setKomentar(komentar);
            review.setPhoto("");
            // ini kalo ga review berarti hapus aja
//            DatabaseReference dbReview = mDatabase.child("review");
            DatabaseReference dbReview = mDatabase;
            //update data
            dbReview.child(reviewId).setValue(review);

            finish();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form, menu);

        return super.onCreateOptionsMenu(menu);
    }

    //pilih menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                showAlertDialog(ALERT_DIALOG_DELETE);
                break;
            case android.R.id.home:
                showAlertDialog(ALERT_DIALOG_CLOSE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showAlertDialog(ALERT_DIALOG_CLOSE);
    }

    private void showAlertDialog(int type) {
        final boolean isDialogClose = type == ALERT_DIALOG_CLOSE;
        String dialogTitle, dialogMessage;

        if (isDialogClose) {
            dialogTitle = "Batal";
            dialogMessage = "Apakah anda ingin membatalkan perubahan pada form";
        } else {
            dialogTitle = "Hapus Data";
            dialogMessage = "Apakah anda yakin ingin menghapus item ini";
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle(dialogTitle);
        alertDialogBuilder.setMessage(dialogMessage)
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (isDialogClose) {
                            finish();
                        } else {
                            //hapus data
                            DatabaseReference dbReview = mDatabase.child(reviewId);

                            dbReview.removeValue();

                            Toast.makeText(UpdateActivity.this, "Deleting data...", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }).setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}