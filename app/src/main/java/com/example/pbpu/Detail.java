package com.example.pbpu;

public class Detail {
    private String id;
    private String idKota;
    private String kategori;
    private String website;
    private String kontak;
    private String deskripsi;
    private String lokasi;
    private String jarak;
    private String image;

    public Detail() {
    }

    public Detail(String id, String idKota, String kategori, String website, String kontak, String deskripsi, String lokasi, String jarak, String image) {
        this.id = id;
        this.idKota = idKota;
        this.kategori = kategori;
        this.website = website;
        this.kontak = kontak;
        this.deskripsi = deskripsi;
        this.lokasi = lokasi;
        this.jarak = jarak;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdKota() {
        return idKota;
    }

    public void setIdKota(String idKota) {
        this.idKota = idKota;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
