package com.example.pbpu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.pbpu.R;
import com.example.pbpu.List;
import java.util.ArrayList;

public class ListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<List> reviewList = new ArrayList<>();

    public void setReviewList(ArrayList<List> reviewList) {
        this.reviewList = reviewList;
    }

    public ListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return reviewList.size();
    }

    @Override
    public Object getItem(int i) {
        return reviewList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View itemView = view;

        if (itemView == null) {
            itemView = LayoutInflater.from(context)
                    .inflate(R.layout.item_kota, viewGroup, false);
        }

        ViewHolder viewHolder = new ViewHolder(itemView);

        List review = (List) getItem(i);
        viewHolder.bind(review);
        return itemView;
    }

    private class ViewHolder {
        private TextView txtEmail, txtKomentar;

        ViewHolder(View view) {
            txtKomentar = view.findViewById(R.id.txt_komentar);
            txtEmail = view.findViewById(R.id.txt_email);
        }

        void bind(List review) {
            txtKomentar.setText(review.getKomentar());
            txtEmail.setText(review.getEmail());
        }
    }
}
