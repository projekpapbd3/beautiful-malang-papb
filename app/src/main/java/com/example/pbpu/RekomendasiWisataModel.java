package com.example.pbpu;

import android.widget.ImageView;

public class RekomendasiWisataModel {

    String id, idKota, jarak, deskripsi, image;

    public RekomendasiWisataModel() {
    }

    public RekomendasiWisataModel(String id, String idKota, String jarak, String deskripsi, String image) {
        this.id = id;
        this.idKota = idKota;
        this.jarak = jarak;
        this.deskripsi = deskripsi;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getIdKota() {
        return idKota;
    }

    public String getJarak() {
        return jarak;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getImage() {
        return image;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIdKota(String idKota) {
        this.idKota = idKota;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
