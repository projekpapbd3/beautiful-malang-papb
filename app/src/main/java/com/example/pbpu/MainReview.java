package com.example.pbpu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import com.example.pbpu.adapter.ReviewAdapter;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainReview extends AppCompatActivity implements View.OnClickListener {
    private ListView listView;
    private ImageButton btnAdd;
    private ImageView btnBack;

    public static final String EXTRA_REVIEW = "extra_review";
    private Review review;

    private ReviewAdapter adapter;
    private ArrayList<Review> reviewList;
    DatabaseReference dbReview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

//        review = getIntent().getParcelableExtra(EXTRA_REVIEW);
//
//        if (review != null) {
//            path = review.getId();
//        } else {
//            review = new Review();
//        }

        Intent in = getIntent();
        String kota = in.getStringExtra("ID_KOTA");
        String wisata = in.getStringExtra("ID_WISATA");

        String path = "/Kota/"+ kota +"/list/"+ wisata +"/review";


        dbReview = FirebaseDatabase.getInstance().getReference(path);

        listView = findViewById(R.id.lv_list);
        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnBack = findViewById(R.id.back);
        btnBack.setOnClickListener(this);

        reviewList = new ArrayList<>();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainReview.this, UpdateActivity.class);
                intent.putExtra(UpdateActivity.EXTRA_REVIEW, reviewList.get(i));
                intent.putExtra("path", path);

                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent intent = getIntent();
        String kota = intent.getStringExtra("ID_KOTA");
        String wisata = intent.getStringExtra("ID_WISATA");

        String path = "/Kota/"+ kota +"/list/"+ wisata +"/review";
        if (view.getId() == R.id.btn_add) {
            Intent in = new Intent(MainReview.this, CreateActivity.class);
            in.putExtra("path", path);
            startActivity(in);
        } else if(view.getId() == R.id.back) {
            Intent in = new Intent(MainReview.this, DetailWisataActivity.class);
            startActivity(in);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        String email = currentUser.getEmail();
        dbReview.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                reviewList.clear();

                for (DataSnapshot reviewSnapshot : dataSnapshot.getChildren()) {
                    Review review = reviewSnapshot.getValue(Review.class);
                    reviewList.add(review);
                }

                ReviewAdapter adapter = new ReviewAdapter(MainReview.this);
                adapter.setReviewList(reviewList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(MainReview.this, "Terjadi kesalahan.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}

