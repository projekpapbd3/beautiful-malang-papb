package com.example.pbpu;

import android.os.Parcel;
import android.os.Parcelable;

public class List implements Parcelable {
    private String id;
    private String idKota;
    private String email;
    private String komentar;
    private String photo;

    public List() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdKota() {
        return idKota;
    }

    public void setIdKota(String idKota) {
        this.idKota = idKota;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.idKota);
        dest.writeString(this.email);
        dest.writeString(this.komentar);
        dest.writeString(this.photo);
    }

    protected List(Parcel in) {
        this.id = in.readString();
        this.idKota = in.readString();
        this.email = in.readString();
        this.komentar = in.readString();
        this.photo = in.readString();
    }

    public static final Parcelable.Creator<List> CREATOR = new Parcelable.Creator<List>() {
        @Override
        public List createFromParcel(Parcel source) {
            return new List(source);
        }

        @Override
        public List[] newArray(int size) {
            return new List[size];
        }
    };
}
